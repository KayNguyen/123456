<?php

/*
 * Api
 * */


Route::group(['prefix' => 'xx_kayn_a/api/v1'], function() {
    Route::any('events', ['uses' => 'API\events_api@index']);
    Route::any('organizers/{organizerSlug}/events/{eventSlug}/{registration?}', ['uses' => 'API\organizers_api@detail']);
    Route::any('organizers', ['uses' => 'API\events_api@organizers']);
    Route::post('login', ['uses' => 'API\login_api@login']);
    Route::post('logout', ['uses' => 'API\login_api@logout']);
    Route::any('{api_class?}/{api_func?}', ['uses' => 'API\AppApi@public_api']);
});

/*
 * Quản trị Admin
 */
Route::get('/admin/login', ['as' => 'admin.login', 'uses' => 'BackEnd\Auth\LoginController@showLoginForm']);
Route::post('/admin/login', ['as' => 'admin.login', 'uses' => 'BackEnd\Auth\LoginController@login']);
Route::get('/admin/register', ['as' => 'admin.register', 'uses' => 'BackEnd\Auth\RegisterController@showRegisterForm']);
Route::post('/admin/register', ['as' => 'admin.register', 'uses' => 'BackEnd\Auth\RegisterController@register']);
Route::get('/admin/logout', ['as' => 'admin.logout', 'uses' => 'BackEnd\Auth\LoginController@logout']);

Route::group(['middleware' => ['auth:admin'], 'prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'BackEnd'], function () {
    Route::any('/', ['as' => 'home', 'uses' => 'Home\HomeController@index']);

    Route::group(['prefix' => 'event', 'as' => 'event.'], function () {
        Route::get('/', ['uses' => 'Events\EventsController@input']);
        Route::get('/input{id?}', ['as' => 'input', 'uses' => 'Events\EventsController@input']);
        Route::get('/detail-{id}', ['as' => 'detail', 'uses' => 'Events\EventsController@detail']);
        Route::post('/_save', ['as' => 'save', 'uses' => 'Events\EventsController@_save']);


    });
    Route::group(['prefix' => 'ticket', 'as' => 'ticket.'], function () {
        Route::get('/', ['uses' => 'Tickets\TicketsController@input']);
        Route::get('/input{id?}', ['as' => 'input', 'uses' => 'Tickets\TicketsController@input']);
        Route::get('/detail-{id}', ['as' => 'detail', 'uses' => 'Tickets\TicketsController@detail']);
        Route::post('/_save', ['as' => 'save', 'uses' => 'Tickets\TicketsController@_save']);


    });
    Route::group(['prefix' => 'channel', 'as' => 'channel.'], function () {
        Route::get('/input{id?}', ['as' => 'input', 'uses' => 'Channels\ChannelsController@input']);
        Route::post('/_save', ['as' => 'save', 'uses' => 'Channels\ChannelsController@_save']);
    });
    Route::group(['prefix' => 'room', 'as' => 'room.'], function () {
        Route::get('/input{id?}', ['as' => 'input', 'uses' => 'Rooms\RoomsController@input']);
        Route::get('/chart', ['as' => 'chart', 'uses' => 'Rooms\RoomsController@chart']);
        Route::post('/_save', ['as' => 'save', 'uses' => 'Rooms\RoomsController@_save']);
    });
    Route::group(['prefix' => 'session', 'as' => 'session.'], function () {
        Route::get('/input{id?}', ['as' => 'input', 'uses' => 'Sessions\SessionsController@input']);
        Route::post('/_save', ['as' => 'save', 'uses' => 'Sessions\SessionsController@_save']);
    });

});

/*
 * Giao diện client
 */
Auth::routes();
Route::get('/', ['as' => 'home', 'uses' => 'FrontEnd\Home\HomeController@index']);
Route::any('event/{action_name?}', ['as' => 'event', 'uses' => 'FrontEnd\Event\EventController@index']);
Route::group(['middleware' => ['auth:web'], 'prefix' => '', 'namespace' => 'FrontEnd'], function () {
    Route::any('register', ['as' => 'event', 'uses' => 'Event\EventController@register']);
});
