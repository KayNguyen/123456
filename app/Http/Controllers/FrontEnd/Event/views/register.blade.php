@extends('FrontEnd.layouts')

@section('CONTENT_REGION')
    <div class="container">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <h3 class="d-inline-block">{{ @$event['name'] }}</h3>
            </div>

            <div class="card-body pb-0">
                <div class="row">
                    @foreach($event['tickets'] as $t)
                    <div class="col ticket">
                        <div class="border card-body">
                            <input type="checkbox" class="checkticket" data-ticketprice="{{ @$t['cost'] }}" @if($loop->first) checked @endif>
                            {{ @$t['name'] }}
                            <span class="float-right">{{ @$t['cost'] }}</span>
                        </div>
                    </div>
                    @endforeach
                </div>

                <h6 class="mt-2">Select addtional workshops you want to book</h6>
                <div class="row">
                    <div class="col-8">
                        @php
                            $costSess = 0;
                        @endphp
                        @if(@$event['channels'])
                            @foreach($event['channels'] as $c)
                                @if(@$c['rooms'])
                                    @foreach($c['rooms'] as $r)
                                        @if(@$r['sessions'])
                                            @foreach($r['sessions'] as $s)
                                                @if($s['type'] == 'workshop')
                                                    <div class="px-0 col-12 workshop">
                                                        <div class="border card-body">
                                                            <input type="checkbox" class="checkworkshop" data-workshopprice="{{ @$s['cost'] }}"  @if($loop->first) @php $costSess = @$s['cost'] @endphp  checked @endif>
                                                            {{ @$s['title'] }}
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="row justify-content-end">
                    <div class="col-3">
                        <h5>Event ticket: <span class="float-right total_ticket">{{ @$event['tickets'][0]['cost'] }}</span></h5>
                        <h5>Addtional workshops: <span class="float-right total_workshops">{{ $costSess }}</span></h5>
                        <hr>
                        <b>Total: <span class="float-right total_all">{{ $costSess+@$event['tickets'][0]['cost'] }}</span></b>
                    </div>
                    <div class="col-12">
                        <button class="float-right my-3 btn btn-outline-success">Purchase</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('JS_REGION')
    <script>
        $(function () {
            $('.checkticket').click(function () {
                console.log(123)
                var $boxes = $('.checkticket:checked');
                var $workShop = $('.checkworkshop:checked');
                let total_ticket = 0;
                $boxes.each(function(i, v){
                    total_ticket += Number($(this).data('ticketprice'));
                });
                $('.total_ticket').text(total_ticket);
                $workShop.each(function(i, v){
                    total_ticket += Number($(this).data('workshopprice'));
                });
                $('.total_all').text(total_ticket);
            })
            $('.checkworkshop').click(function () {
                console.log(123)
                var $boxes = $('.checkworkshop:checked');
                var $workShop = $('.checkticket:checked');
                let total_ticket = 0;
                $boxes.each(function(i, v){
                    total_ticket += Number($(this).data('workshopprice'));
                });
                $('.total_workshops').text(total_ticket);
                $workShop.each(function(i, v){
                    total_ticket += Number($(this).data('ticketprice'));
                });
                $('.total_all').text(total_ticket);
            })
        });
    </script>
@endpush
