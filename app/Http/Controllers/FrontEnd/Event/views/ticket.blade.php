@extends('FrontEnd.layouts')

@section('CONTENT_REGION')
    <div class="container">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <h3 class="d-inline-block">{{ @$obj['title'] }}</h3>
            </div>

            <div class="card-body pb-0">
                <p>{{ @$obj['description'] }}</p>
                <div class="d-flex flex-column">
                    <div>
                        <strong>Speaker: </strong> {{ @$obj['speaker']?:"Chưa cập nhật" }}
                    </div>
                    <div>
                        <strong>Start: </strong> {{ @$obj['start']?:"Chưa cập nhật" }}
                    </div>
                    <div>
                        <strong>End: </strong> {{ @$obj['end']?:"Chưa cập nhật" }}
                    </div>
                    <div>
                        <strong>Cost: </strong> {{ @$obj['cost']?:"Chưa cập nhật" }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
