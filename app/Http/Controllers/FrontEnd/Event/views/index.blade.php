@extends('FrontEnd.layouts')

@section('CONTENT_REGION')
    <div class="container">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <h3 class="d-inline-block">{{ $obj['name'] }}</h3>
                <form action="{{ public_link('register?id='.$obj['id']) }}" method="POST">
                    @csrf
                    <button class="register btn btn-success">Đăng ký sự kiện</button>
                </form>
            </div>

            <div class="card-body pb-0">
                <div class="row">
                    <div class="col-md-4">

                    </div>
                    <div class="col-md-8">
                        @foreach($start_times as $time)
                        <a href="">{{ $time }}</a>
                        @endforeach
                    </div>
                </div>
            @foreach($obj->channels as $channel)
               <div class="row">
                   <div class="col-md-2 box">
                       <div class="channel">
                           <a href="">{{ $channel['name'] }}</a>
                       </div>
                   </div>
                   <div class="col-md-2 box">
                       <div class="row">
                           @foreach($channel->rooms as $room)
                               <div class="col-md-12 room">
                                   <a href="">{{ $room['name'] }}</a>
                               </div>
                           @endforeach
                       </div>
                   </div>
                   <div class="col-md-8 box">
                       <div class="row">
                           @foreach($channel->rooms as $room)
                           <div class="col-md-12 session">
                               @foreach($room->sessions as $session)
                               <a href="{{ \Auth::check() ? public_link('register?id='.$session['id']) : public_link('event/session?id='.$session['id']) }}">{{ $session['title'] }}</a>
                               @endforeach
                           </div>
                           @endforeach
                       </div>
                   </div>
               </div>
            @endforeach
            </div>
        </div>
    </div>
@endsection
