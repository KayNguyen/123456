<?php


namespace App\Http\Controllers\FrontEnd\Event;


use App\Http\Controllers\Controller;
use App\Http\Models\Event;
use App\Http\Models\Registration;
use App\Http\Models\Session;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function index($action = '') {
        $action = str_replace('-', '_', $action);
        if(method_exists($this, $action)) {
            return $this->$action();
        }
        return $this->event();
    }

    public function event() {
        $tpl = [];
        $alias = Request::capture()->input('alias', 0);
        $tpl['title_site'] = 'Quản trị sự kiện';
        $obj = Event::getDetailEventByAlias($alias);
        $sessions = Event::getAllSessionOfEventComing($obj->id);
        //$registered = Registration::getIdSessionRegistered();
        $times = [];
        foreach ($sessions as $time => $session) {
            $times[] = date('H', strtotime($time));
            unset($sessions[$time]);
        }
        $tpl['start_times'] = $times;
        if($obj) {
            $tpl['obj'] = $obj;
            return \Lib::getInstance()->setView(__DIR__, 'index', $tpl);
        }
        return abort('404');
    }

    public function session() {
        $tpl = [];
        $id = Request::capture()->input('id', 0);
        $tpl['title_site'] = 'Chi tiết session';
        $obj = Session::getDetailSessionById($id);
        if($obj) {
            $tpl['obj'] = $obj;
            return \Lib::getInstance()->setView(__DIR__, 'session', $tpl);
        }
        return abort('404');
    }

    public function register() {
        $id = Request::capture()->input('id', 0);
        $event = Event::getDetailEventById($id);
        if($event) {
            $tpl['title_site'] = 'Đăng ký sự kiện';
            $tpl['event'] = $event;
            return \Lib::getInstance()->setView(__DIR__, 'register', $tpl);
        }
        return redirect()->route('event');
    }
}
