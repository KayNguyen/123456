<?php

namespace App\Http\Controllers\FrontEnd\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Event;

class HomeController extends Controller
{
    public function index($action = '') {
        $action = str_replace('-', '_', $action);
        if(method_exists($this, $action)) {
            return $this->$action;
        }
        return $this->home();
    }

    function home() {
        $tpl = [];
        $per = 10;
        $tpl['title_site'] = 'Quản trị sự kiện';
        $tpl['lsEvents']  = Event::getAllUpcomingEvent($per);
        return \Lib::getInstance()->setView(__DIR__, 'index', $tpl);
    }
}
