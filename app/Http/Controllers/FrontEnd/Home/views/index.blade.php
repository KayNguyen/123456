@extends('FrontEnd.layouts')

@section('CONTENT_REGION')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="list-group">
                    @foreach($lsEvents as $event)
                    <a href="{{ public_link('event?alias='.$event['slug']?:str_slug($event['name'])) }}" class="list-group-item list-group-item-action">

                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">{{ $event['name'] }}</h5>
                        </div>
                        <small>{{ $event->organizers->name .' - '. $event['date'] }}</small>
                    </a>
                    @endforeach
                    @if(empty($lsEvents) || $lsEvents->isEmpty())
                        <div class="alert alert-danger" role="alert">
                            <i class="mdi mdi-block-helper mr-2"></i>
                            Không tìm thấy dữ liệu nào ở trang này. (Hãy kiểm tra lại các điều kiện tìm kiếm hoặc phân trang...)
                        </div>
                    @else
                        {!! $lsEvents->links('FrontEnd.pagination', ['data' => $lsEvents]) !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
