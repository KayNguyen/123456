<?php

namespace App\Http\Controllers\BackEnd\Channels;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChannelRequest;
use App\Http\Models\Channel as THIS;

class ChannelsController extends Controller
{
    public function input()
    {
        $tpl = [];
        $tpl['site_title'] = 'Cập nhật channel';

        $id = Request::capture()->input('id', 0);
        if($id) {
            $tpl['data'] = THIS::getDetailEventById($id);

            $tpl['site_title'] = 'Cập nhật channel - '.$tpl['data']['name'];
        }
        return \Lib::getInstance()->setView(__DIR__, 'input', $tpl);
    }


    public function _save(ChannelRequest $request)
    {
        $tpl = [];
        $id = $request->id;
        $obj['name'] = $request->name;
        $obj['event_id'] = session()->get('event_id');
        $model = THIS::find($id);
        if(!$model) {
            // Thêm mới


            $id = THIS::insertGetId($obj);
            if($id) {
                return redirect()->route('admin.event.detail', ['id' => $obj['event_id']]);
            }
        }else {
            $tpl['site_title'] = 'Cập nhật channel';
            $model->name = $obj['name'];
            $model->event_id = $obj['event_id'];
            $model->save();
            return redirect()->route('admin.channel.input', ['id' => '?id='.$model['id']]);
        }
        return \Lib::setView(__DIR__, 'input', $tpl);
    }
}
