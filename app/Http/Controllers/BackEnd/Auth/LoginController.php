<?php

namespace App\Http\Controllers\BackEnd\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{

    protected $redirectTo = '/admin/';

    protected function guard()
    {
        return Auth::guard('admin');
    }

    public function showLoginForm() {
        $tpl = [];
        $tpl['site_title'] = 'Đăng nhập vào trang quản trị';
        $tpl['loginRoute'] = 'admin.login';
        $tpl['forgotPasswordRoute'] = 'admin.password.request';
        return \Lib::getInstance()->setView(__DIR__, 'login', $tpl);
    }

    public function validator(Request $request) {
        $rules = [
            'email' => 'required|email|exists:organizers|min:5|max:191',
            'password' => 'required|string|min:4|max:255',
        ];

        $msg = [
            'email.exists' => 'Email không tồn tại! Vui lòng thử lại!',
            'password.required' => 'Mật khẩu không được để trống! Vui lòng thử lại!',
            'password.min' => 'Mật khẩu phải tối thiểu 4 kí tự! Vui lòng thử lại!',
            'password.max' => 'Mật khẩu phải tối thiểu 255 kí tự! Vui lòng thử lại!',
        ];

        $request->validate($rules, $msg);

    }

    public function loginFailed() {
        return redirect()->back()->withInput()->with('error', 'Đăng nhập thất bại! Vui lòng thử lại!');
    }


    public function login(Request $request) {
        $this->validator($request);
        if(\Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect()->intended(route('admin.home'));
        }
        return $this->loginFailed();
    }

    public function logout(Request $request)
    {

        \Auth::guard('admin')->logout();

//        $request->session()->invalidate();

        return redirect('admin/login');
    }
}
