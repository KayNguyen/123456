<?php

namespace App\Http\Controllers\BackEnd\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Models\Admin;


class RegisterController extends Controller
{
    public function showRegisterForm() {
        $tpl = [];
        $tpl['site_title'] = 'Đăng kí thành viên quản trị';
        return \Lib::getInstance()->setView(__DIR__, 'register', $tpl);
    }

    public function validator(Request $request) {
        $rules = [
            'name' => 'required|string|min:4|max:255|unique:organizers',
            'email' => 'required|email|max:255|unique:organizers',
            'password' => 'required|string|min:6|confirmed'
        ];

        $msg = [
          'email.unique' => 'Email đã tồn tại! Vui lòng thử lại!'
        ];

        $request->validate($rules, $msg);
    }

    public function registerFailed() {
        return redirect()->route('admin.register')->withInput()->with('error', 'Đăng kí không thành công. Vui lòng thử lại!');
    }

    public function register(Request $request) {

        $this->validator($request);
        $admin = Admin::create([
            'name' => $request->name,
            'email' => $request->email,
            'slug' => str_slug($request->name),
            'password_hash' => Hash::make($request->password),
            'updated_at' => time(),
        ]);
        if($admin) {
            return redirect()->route('admin.login')->withInput()->with('success', 'Đăng kí thành công. Vui lòng đăng nhập để truy cập hệ thống!');;
        }
        return $this->registerFailed();
    }
}
