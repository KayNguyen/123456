@extends('BackEnd.layouts')

@section('CONTENT')
    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                        <li class="nav-item"><a class="nav-link active" href="{{ admin_link('') }}">Manage Events</a></li>
                    </ul>
                </div>
            </nav>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <div class="border-bottom mb-3 pt-3 pb-2">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                        <h1 class="h2">{{ @$event['name'] }}</h1>
                    </div>
                    <span class="h6">{{ @$event['date'] }}</span>
                </div>

                <div class="mb-3 pt-3 pb-2">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                        <h2 class="h4">Create new ticket</h2>
                    </div>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="needs-validation" action="{{ route('admin.ticket.save') }}" method="POST">
                    @csrf
                    <input type="hidden" name="event_id" value="{{ @$event_id }}">
                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            <label for="inputName">Name</label>
                            <!-- adding the class is-invalid to the input, shows the invalid feedback below -->
                            <input type="text" class="form-control is-invalid" id="inputName" name="name" placeholder="" value="{{ @$data['name'] }}">
                            <div class="invalid-feedback">
                                Name is required.
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            <label for="inputCost">Cost</label>
                            <input type="number" class="form-control" id="inputCost" name="cost" placeholder="" value="{{ @$data['cost'] }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            <label for="selectSpecialValidity">Special Validity</label>
                            <select class="form-control" id="selectSpecialValidity" name="special_validity">
                                <option value="" selected>None</option>
                                <option @if(@$data['special_validity']->type == 'amount') selected @endif value="amount">Limited amount</option>
                                <option @if(@$data['special_validity']->type == 'date') selected @endif value="date">Purchaseable till date</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            <label for="inputAmount">Maximum amount of tickets to be sold</label>
                            <input type="number" class="form-control" id="inputAmount" name="amount" placeholder="" value="{{ @$data['special_validity']->amount}}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            <label for="inputValidTill">Tickets can be sold until</label>
                            <input type="text"
                                    class="form-control"
                                    id="inputValidTill"
                                    name="valid_until"
                                    placeholder="yyyy-mm-dd HH:MM"
                                    value="{{ @$data['special_validity']->date}}">
                        </div>
                    </div>

                    <hr class="mb-4">
                    <button class="btn btn-primary" type="submit">Save ticket</button>
                    <a href="events/detail.html" class="btn btn-link">Cancel</a>
                </form>

            </main>
        </div>
    </div>
@stop
