<?php

namespace App\Http\Controllers\BackEnd\Rooms;

use App\Http\Requests\RoomRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Room as THIS;
use App\Http\Models\Channel;

class RoomsController extends Controller
{
    public function input()
    {
        $tpl = [];
        $tpl['site_title'] = 'Thêm mới room';
        $id = Request::capture()->input('id', 0);
        if(session()->has('channel_ids')) {
            $channel_ids = session()->get('channel_ids');
        }
        if ($channel_ids) {
            $tpl['channels'] = Channel::getChannelByIds($channel_ids);
            if($id) {
                $tpl['data'] = THIS::getDetailRoomById($id);

                $tpl['site_title'] = 'Cập nhật channel - '.$tpl['data']['name'];
            }
            return \Lib::getInstance()->setView(__DIR__, 'input', $tpl);
        }

    }


    public function _save(RoomRequest $request)
    {
        $tpl = [];
        $id = $request->id;
        $obj['name'] = $request->name;
        $obj['channel_id'] = $request->channel;
        $obj['capacity'] = $request->capacity;
        $event_id = session()->get('event_id');
        $model = THIS::find($id);
        if(!$model) {
            // Thêm mới
            $id = THIS::insertGetId($obj);
            if($id) {
                return redirect()->route('admin.event.detail', ['id' => $event_id]);
            }
        }else {
            $tpl['site_title'] = 'Cập nhật channel';
            $model->name = $obj['name'];
            $model->event_id = $obj['event_id'];
            $model->save();
            return redirect()->route('admin.channel.input', ['id' => '?id='.$model['id']]);
        }
        return \Lib::setView(__DIR__, 'input', $tpl);
    }

    /**
     * Biểu đồ sức chứa của phòng của từng event
     */

    public function chart() {
        $tpl = [];
        if(session()->has('event_id')) {
            $data = THIS::getRoomsByEventId(session()->get('event_id'));
            if (count($data['channels']) > 0) {
                foreach ($data['channels'] as $channel) {
                    if (count($channel['rooms']) > 0) {
                        foreach ($channel['rooms'] as $room) {
                            $tpl['rooms'][$room['id']] = [
                                'id' => $room['id'],
                                'name' => $room['name'],
                                'capacity' => $room['capacity']
                            ];
                        }
                    }
                }
            }
        }

        $tpl['site_title'] = 'Báo cáo thống kê phòng';
        return \Lib::getInstance()->setView(__DIR__, 'chart', $tpl);
    }


}
