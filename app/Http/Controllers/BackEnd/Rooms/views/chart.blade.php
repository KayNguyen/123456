@extends('BackEnd.layouts')

@section('CONTENT')
    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                        <li class="nav-item"><a class="nav-link active" href="{{ admin_link('') }}">Manage Events</a></li>
                    </ul>
                </div>
            </nav>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <div
                    class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">Room Capacity</h1>
                </div>

                <div class="mb-3 pt-3 pb-2">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                        <h2 class="h4">Room Capacity</h2>
                    </div>
                </div>
                <canvas id="r-i-p" width="300" height="100"></canvas>
            </main>
        </div>
    </div>
@stop

@section('JS_TOP')
    <script src="{{ asset('auth/assets/js/chart.bundle.min.js') }}"></script>
    <script>
        var ctx = document.getElementById('r-i-p').getContext('2d'),
        object = JSON.parse('{!! json_encode($rooms) !!}'), capacity = [], attendees = [10, 100, 90], labels = [];
        for (const key in object) {
            if (object.hasOwnProperty(key)) {
                capacity.push(object[key]['capacity']);
                labels.push(object[key]['name']);
            }
        }
        console.log(capacity, labels)
        

        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: labels,
                datasets: [{
                    label: 'Capacity',
                    data: capacity,
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1
                },
                {
                    label: 'Attendees',
                    data: attendees,
                    backgroundColor: '#28a745',
                    borderColor: '#28a745',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
@stop
