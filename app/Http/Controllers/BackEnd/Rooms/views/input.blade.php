@extends('BackEnd.layouts')

@section('CONTENT')
    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                        <li class="nav-item"><a class="nav-link active" href="{{ route('admin.event.detail', ['id' => session()->get('event_id')]) }}">Manage Events</a></li>
                    </ul>
                </div>
            </nav>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <div
                    class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">Manage Room</h1>
                </div>

                <div class="mb-3 pt-3 pb-2">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                        <h2 class="h4">Create new room</h2>
                    </div>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="needs-validation" method="POST" action="{{ route('admin.room.save') }}">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            <label for="inputName">Name</label>
                            <!-- adding the class is-invalid to the input, shows the invalid feedback below -->
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="inputName" name="name" placeholder="" value="{{ old('name') }}">
                            @error('name')
                            <div class="invalid-feedback">
                                Name is required.
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            <label for="selectChannel">Channel</label>
                            <select class="form-control" id="selectChannel" name="channel">
                                @if(!empty($channels))
                                    @foreach($channels as $channel)
                                        <option value="{{ $channel['id'] }}">{{ $channel['name'] }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            <label for="inputCapacity">Capacity</label>
                            <input type="number" class="form-control" id="inputCapacity" name="capacity" placeholder="" value="">
                            @error('capacity')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <hr class="mb-4">
                    <button class="btn btn-primary" type="submit">Save room</button>
                    <a href="{{ route('admin.event.detail', ['id' => session()->get('event-id')]) }}" class="btn btn-link">Cancel</a>
                </form>

            </main>
        </div>
    </div>
@stop
