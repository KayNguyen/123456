<?php

namespace App\Http\Controllers\BackEnd\Home;

use App\Http\Models\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index($action = '') {
        $action = str_replace('-', '_', $action);
        if(method_exists($this, $action)) {
            return $this->$action;
        }
        return $this->home();
    }

    function home() {
        $tpl = [];
        $user = \Auth::guard('admin')->user();
        $tpl['site_title'] = 'Danh sách sự kiện';
        $tpl['breadcrumb'] = $user->name;
        $tpl['events']  = Event::getAllEventByIdOrganizer($user->id);;
        return \Lib::getInstance()->setView(__DIR__, 'index', $tpl);
    }
}
