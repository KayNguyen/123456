<?php

namespace App\Http\Controllers\BackEnd\Sessions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SessionRequest;
use App\Http\Models\Session as THIS;
use App\Http\Models\Channel;

class SessionsController extends Controller
{
    public function input()
    {
        $tpl = [];
        $tpl['site_title'] = 'Thêm mới session';
        $id = Request::capture()->input('id', 0);
        if(session()->has('channel_ids')) {
            $channel_ids = session()->get('channel_ids');
        }else {
            return redirect()->back();
        }
        if ($channel_ids) {
            $tpl['channels'] = Channel::getRoomAndChannelByIds($channel_ids);
            if (count($tpl['channels']) > 0) {
                foreach ($tpl['channels'] as $channel) {
                    if (count($channel['rooms']) > 0) {
                        foreach ($channel['rooms'] as $room) {
                            $tpl['rooms'][$room['id']] = [
                                'id' => $room['id'],
                                'name' => $room['name'] . '-' . $channel['name'],
                                'capacity' => $room['capacity']
                            ];
                        }
                    }
                }
            }
            if($id) {
                if(session()->has('event_id')) {
                    $event_id = session()->get('event_id');
                }
//                dd(THIS::checkIsYourSessionById($id));
                /*if () {
                    // Kiểm tra xem session có thuộc admin_id hay ko
                }*/
                $tpl['data'] = THIS::getDetailSessionById($id);

                $tpl['site_title'] = 'Cập nhật channel - '.$tpl['data']['title'];
            }
            return \Lib::getInstance()->setView(__DIR__, 'input', $tpl);
        }

    }


    public function _save(SessionRequest $request)
    {
        $tpl = [];
        $id = $request->id;
        $obj['title'] = $request->title;
        $obj['room_id'] = $request->room;
        $obj['description'] = $request->description;
        $obj['speaker'] = $request->speaker;
        $obj['start'] = $request->start;
        $obj['end'] = $request->end;
        $obj['type'] = $request->type;
        $obj['cost'] = $request->cost;
        $event_id = session()->get('event_id');
        $model = THIS::find($id);
        if(!$model) {
            // Thêm mới
            $id = THIS::insertGetId($obj);
            if($id) {
                return redirect()->route('admin.event.detail', ['id' => $event_id]);
            }
        }else {
            $tpl['site_title'] = 'Cập nhật session';
            $model->title = $obj['title'];
            $model->room_id = $obj['room_id'];
            $model->description = $obj['description'];
            $model->speaker = $obj['speaker'];
            $model->start = $obj['start'];
            $model->end = $obj['end'];
            $model->type = $obj['type'];
            $model->cost = $obj['cost'];
            $model->save();
            return redirect()->route('admin.session.input', ['id' => '?id='.$model['id']]);
        }
        return \Lib::setView(__DIR__, 'input', $tpl);
    }
}
