@extends('BackEnd.layouts')

@section('CONTENT')
    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                        <li class="nav-item"><a class="nav-link active" href="{{ route('admin.event.detail', ['id' => session()->get('event_id')]) }}">Manage Events</a></li>
                    </ul>
                </div>
            </nav>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <div
                    class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">Manage session</h1>
                </div>

                <div class="mb-3 pt-3 pb-2">
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                        <h2 class="h4">Create new session</h2>
                    </div>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="needs-validation" method="POST" action="{{ route('admin.session.save', ['id' => @$data['id']]) }}">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            <label for="selectType">Type</label>
                            <select class="form-control" id="selectType" name="type">
                                <option value="talk" {{ (@$data['type'] == 'talk') ? 'selected' : '' }}>Talk</option>
                                <option value="workshop" {{ (@$data['type'] == 'workshop') ? 'selected' : '' }}>Workshop</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            <label for="inputTitle">Title</label>
                            <!-- adding the class is-invalid to the input, shows the invalid feedback below -->
                            <input type="text" required class="form-control" id="inputTitle" name="title" placeholder="" value="{{ old('title', @$data['title']) }}">
                            {{--<div class="invalid-feedback">
                                Title is required.
                            </div>--}}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            <label for="inputSpeaker">Speaker</label>
                            <input type="text" required class="form-control" id="inputSpeaker" name="speaker" placeholder="" value="{{ old('speaker', @$data['speaker']) }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            <label for="selectRoom">Room</label>
                            <select class="form-control" id="selectRoom" name="room">
                                @if(!empty($rooms))
                                    @foreach($rooms as $room)
                                        <option value="{{ $room['id'] }}" {{ (@$data['room_id'] == $room['id']) ? 'selected' : '' }}>{{ $room['name'] }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            <label for="inputCost">Cost</label>
                            <input type="number" required class="form-control" id="inputCost" name="cost" placeholder="" value="{{ old('cost', @$data['cost']) }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-lg-4 mb-3">
                            <label for="inputStart">Start</label>
                            <input type="datetime-local"
                                   class="form-control"
                                   id="inputStart"
                                   name="start"
                                   value="{{ old('start',  date('Y-m-d\TH:i', strtotime(@$data['start']?:date('Y-m-d\TH:i')))) }}">
                        </div>
                        <div class="col-12 col-lg-4 mb-3">
                            <label for="inputEnd">End</label>
                            <input type="datetime-local"
                                   class="form-control"
                                   id="inputEnd"
                                   name="end"
                                   value="{{ old('end',  date('Y-m-d\TH:i', strtotime(@$data['end']?:date('Y-m-d\TH:i')))) }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 mb-3">
                            <label for="textareaDescription">Description</label>
                            <textarea required class="form-control" id="textareaDescription" name="description" placeholder="" rows="5">{{ old('description', @$data['description']) }}</textarea>
                        </div>
                    </div>

                    <hr class="mb-4">
                    <button class="btn btn-primary" type="submit">Save room</button>
                    <a href="{{ route('admin.event.detail', ['id' => session()->get('event-id')]) }}" class="btn btn-link">Cancel</a>
                </form>

            </main>
        </div>
    </div>
@stop
