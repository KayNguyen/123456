<?php

namespace App\Http\Controllers\BackEnd\Events;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Event as THIS;
use App\Http\Requests\EventRequest;

class EventsController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($action = '')
    {
        $action = str_replace('-', '_', $action);

        if (method_exists($this, $action)) {
            return $this->$action();
        } else {
            return $this->home();
        }
    }

    public function home()
    {
        $tpl = [];
        $tpl['site_title'] = 'Trang quản trị';
        return \Lib::getInstance()->setView(__DIR__, 'index', $tpl);
    }

    public function input()
    {
        $tpl = [];
        $tpl['site_title'] = 'Cập nhật sự kiện';

        $id = Request::capture()->input('id', 0);
        if($id) {
            $tpl['data'] = THIS::getDetailEventById($id);

            $tpl['site_title'] = 'Cập nhật sự kiện - '.$tpl['data']['name'];
        }
        return \Lib::getInstance()->setView(__DIR__, 'input', $tpl);
    }

    public function detail($id)
    {
        $tpl = [];
        session()->put('event_id', $id);
        if($id) {
            $tpl['site_title'] = 'Chi tiết sự kiện';
            $tpl['data'] = THIS::getDetailEventById($id);
            if (count($tpl['data']['channels']) > 0) {
                foreach ($tpl['data']['channels'] as $channel) {
                    if (count($channel['rooms']) > 0) {
                        foreach ($channel['rooms'] as $room) {
                            $tpl['data']['rooms'][$room['id']] = [
                                'id' => $room['id'],
                                'name' => $room['name'],
                                'capacity' => $room['capacity']
                            ];
                            if(count($room['sessions']) > 0) {
                                foreach ($room['sessions'] as $session) {
                                    $tpl['data']['session'][$session['id']] = [
                                        'id' => $session['id'],
                                        'title' => $session['title'],
                                        'type' => $session['type'],
                                        'start' => $session['start'],
                                        'end' => $session['end'],
                                        'speaker' => $session['speaker'],
                                        'channel' => $channel['name'] . '--' . $room['name'],
                                    ];
                                }
                            }
                        }
                    }
                }
            }
            $arrChannelSession = [];
            if (session()->has('channel_ids')) {
                $arrChannelSession = session()->get('channel_ids');
            }

            foreach ($tpl['data']['channels'] as $channel) {
                if (!in_array($channel['id'], $arrChannelSession)) {
                    session()->push('channel_ids', $channel['id']);
                }
            }
            return \Lib::getInstance()->setView(__DIR__, 'index', $tpl);
        }
        return abort(404);

    }

    public function _save(EventRequest $request)
    {
        $tpl = [];
        $id = $request->id;
        $obj['name'] = $request->name;
        $obj['slug'] = ($request->slug) ? $request->slug : \str_slug($request->name);
        $obj['date'] = $request->date;
        $obj['organizer_id'] = \Auth::guard('admin')->user()->id;
        $model = THIS::find($id);
        if(!$model) {
            // Thêm mới


            $id = THIS::insertGetId($obj);
            if($id) {
                return redirect()->route('admin.home');
            }
        }else {
            $tpl['site_title'] = 'Cập nhật sự kiện';
            $model->name = $obj['name'];
            $model->slug = $obj['slug'];
            $model->date = $obj['date'];
            $model->save();
            return redirect()->route('admin.event.input', ['id' => '?id='.$model['id']]);
        }
        return \Lib::setView(__DIR__, 'input', $tpl);
    }
}
