@extends('BackEnd.layouts')

@section('CONTENT')
    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                        <li class="nav-item"><a class="nav-link" href="{{ route('admin.home') }}">Manage Events</a></li>
                    </ul>

                    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                            <span>{{ @$data['name'] }}</span>
                        </h6>
                        <ul class="nav flex-column">
                            <li class="nav-item"><a class="nav-link active" href="javascript:void();">Overview</a></li>
                        </ul>
        
                        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                            <span>Reports</span>
                        </h6>
                        <ul class="nav flex-column mb-2">
                            <li class="nav-item"><a class="nav-link" href="{{ route('admin.room.chart') }}">Room capacity</a></li>
                        </ul>
                </div>
            </nav>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                    <div class="border-bottom mb-3 pt-3 pb-2 event-title">
                        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                            <h1 class="h2">{{ @$data['name'] }}</h1>
                            <div class="btn-toolbar mb-2 mb-md-0">
                                <div class="btn-group mr-2">
                                    <a href="{{ route('admin.event.input', ['id' => '?id='.@$data['id']]) }}" class="btn btn-sm btn-outline-secondary">Edit event</a>
                                </div>
                            </div>
                        </div>
                        <span class="h6">{{ @$data['date'] }}</span>
                    </div>

                    <!-- Tickets -->
                    <div id="tickets" class="mb-3 pt-3 pb-2">
                        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                            <h2 class="h4">Tickets</h2>
                            <div class="btn-toolbar mb-2 mb-md-0">
                                <div class="btn-group mr-2">
                                    <a href="{{ route('admin.ticket.input', ['event_id' => '?event_id='.@$data['id']]) }}" class="btn btn-sm btn-outline-secondary">
                                        Create new ticket
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row tickets">
                            @foreach ($data['tickets'] as $ticket)
                            <div class="col-md-4">
                                <div class="card mb-4 shadow-sm">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ @$ticket['name'] }}</h5>
                                        <p class="card-text">200.-</p>
                                        <p class="card-text">&nbsp;</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <!-- Sessions -->
                    <div id="sessions" class="mb-3 pt-3 pb-2">
                        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                            <h2 class="h4">Sessions</h2>
                            <div class="btn-toolbar mb-2 mb-md-0">
                                <div class="btn-group mr-2">
                                    <a href="{{ route('admin.session.input') }}" class="btn btn-sm btn-outline-secondary">
                                        Create new session
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive sessions">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Time</th>
                                <th>Type</th>
                                <th class="w-100">Title</th>
                                <th>Speaker</th>
                                <th>Channel</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($data['session']) && count($data['session'])> 0)
                                @foreach($data['session'] as $session)
                                    <tr>
                                        <td class="text-nowrap">{{ $session['start'] }} - {{ $session['end'] }}</td>
                                        <td class="text-nowrap">{{ $session['type'] }}</td>
                                        <td class="text-nowrap"><a href="{{ route('admin.session.input', ['id' => "?id=".$session['id']]) }}">{{ $session['title'] }}</a></td>
                                        <td class="text-nowrap">{{ $session['speaker'] }}</td>
                                        <td class="text-nowrap">{{ $session['channel'] }}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>

                    <!-- Channels -->
                    <div id="channels" class="mb-3 pt-3 pb-2">
                        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                            <h2 class="h4">Channels</h2>
                            <div class="btn-toolbar mb-2 mb-md-0">
                                <div class="btn-group mr-2">
                                    <a href="{{ route('admin.channel.input') }}" class="btn btn-sm btn-outline-secondary">
                                        Create new channel
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row channels">
                        @foreach($data['channels'] as $channel)
                        <div class="col-md-4">
                            <div class="card mb-4 shadow-sm">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $channel['name'] }}</h5>
                                    <p class="card-text">3 sessions, 1 room</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <!-- Rooms -->
                    <div id="rooms" class="mb-3 pt-3 pb-2">
                        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                            <h2 class="h4">Rooms</h2>
                            <div class="btn-toolbar mb-2 mb-md-0">
                                <div class="btn-group mr-2">
                                    <a href="{{ route('admin.room.input') }}" class="btn btn-sm btn-outline-secondary">
                                        Create new room
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive rooms">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Capacity</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($data['rooms']) && count($data['rooms'])> 0)
                                @foreach($data['rooms'] as $room)
                                    <tr>
                                        <td class="text-nowrap">{{ $room['name'] }}</td>
                                        <td class="text-nowrap">{{ $room['capacity'] }}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>

                </main>
        </div>
    </div>
@stop
