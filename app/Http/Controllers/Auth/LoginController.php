<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $lastname;
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'lastname';
    }

    protected function validateLogin(Request $request)
    {
        $rules = [
            $this->username() => 'required|exists:attendees|max:191',
            'password' => 'required|string|min:4|max:6',
        ];

        $msg = [
            $this->username().'.exists' => 'Tên đăng nhập không tồn tại! Vui lòng thử lại!',
            'password.required' => 'Mật khẩu không được để trống! Vui lòng thử lại!',
            'password.min' => 'Mật khẩu phải tối thiểu 4 kí tự! Vui lòng thử lại!',
            'password.max' => 'Mật khẩu phải tối thiểu 6 kí tự! Vui lòng thử lại!',
        ];

        $request->validate($rules, $msg);
    }


    protected function credentials(Request $request)
    {
        $data  = [$this->username() => $request->lastname, 'password' => $request->password];
        return $data;
    }

    public function logout(Request $request)
    {

        \Auth::logout();

//        $request->session()->invalidate();

        return redirect('/login');
    }

}
