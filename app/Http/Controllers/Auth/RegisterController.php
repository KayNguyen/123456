<?php

namespace App\Http\Controllers\Auth;

use App\Http\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:attendees'],
            'password' => ['required', 'string', 'min:4', 'max:6', 'confirmed'],
        ], [
            'firstname.required' => 'Bạn vui lòng nhập họ của bạn!',
            'firstname.string' => 'Họ của bạn phải dạng chuỗi!',
            'firstname.max' => 'Họ của bạn tối đa 255 ký tự!',
            'lastname.max' => 'Tên của bạn tối đa 255 ký tự!',
            'lastname.required' => 'Bạn vui lòng nhập tên của bạn!',
            'lastname.string' => 'Tên của bạn phải dạng chuỗi!',
            'password.max' => 'Mật khẩu của bạn tối đa 6 ký tự!',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Http\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'lastname' => $data['lastname'],
            'firstname' => $data['firstname'],
            'username' => $data['lastname'],
            'email' => $data['email'],
            'registration_code' => $data['password'],
        ]);
    }
}
