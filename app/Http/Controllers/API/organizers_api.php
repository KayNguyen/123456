<?php

namespace App\Http\Controllers\API;

use App\Http\Models\Event;
use App\Http\Models\Admin;
use App\Http\Models\Registration;
use App\Http\Models\SessionRegistration;
use App\Http\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\API\AppApi;

class organizers_api extends AppApi
{

    public function detail($organizerSlug = '', $eventSlug = '', $registration = '') {
        if($organizerSlug != '' || $eventSlug != '') {
            $organizer = Admin::where('slug', $organizerSlug)->select('id', 'slug')->first();
            if(!empty($organizer)) {
                $events = Event::where([
                    ['slug', $eventSlug],
                    ['organizer_id', $organizer->id],
                ])->select('id', 'slug')->first();
                if(!empty($events)) {
                    $lsObj = Event::getAllDetailEventOfOrganizer($organizer->id, $events->id);
                    if($registration == 'registration') {
                        $token = request('token', 0);
                        if (!$token) {
                            return $this->outputError("Yêu cầu chưa được hỗ trợ", 401);
                        }
                        $existUser = User::where('login_token', $token)->first();
                        if (!$existUser) {
                            return $this->outputError("Thông tin tài khoản không chính xác", 401);
                        }    
                        $ticket_id  = request('ticket_id', 0);
                        $session_ids  = request('session_ids', 0);
                        $registration = Registration::where([
                            ['ticket_id', $ticket_id],
                            ['attendee_id', $existUser->id]
                        ])->first();

                        if($registration) {
                            return $this->outputError("Sự kiện đã được đăng kí", 401);
                        }
                        $regis = new Registration();
                        $regis->ticket_id = $ticket_id;
                        $regis->attendee_id = $existUser->id;
                        $regis->registration_time = date('Y-m-d H:i:s');
                        $regis->save();
                        
                        if(is_array($session_ids)) {
                            foreach($session_ids as $id) {
                                SessionRegistration::create([
                                    'registration_id' => $regis->id,
                                    'session_id' => $id,
                                   ]);
                            }
                        }
                        
                        return $this->outputDone($regis, "Đăng kí sự kiện thành công");
                    }else {
                        return $this->outputError("Yêu cầu chưa được hỗ trợ", 401);
                    }
                    return $this->outputDone($lsObj, "Lấy dữ liệu thành công");
                }
                return $this->outPutError('Không tìm thấy sự kiện!');
            }
            return $this->outPutError('Không tìm thấy nhà tổ chức sự kiện!');
        }
        return $this->outPutError('Không tìm thấy dữ liệu!');
        $lsObj = Event::getAllEventOfOrganizer();
        if(!empty($lsObj)) {
            return $this->outputDone($lsObj, "Lấy dữ liệu thành công");
        }
        return $this->outPutError('Không tìm thấy dữ liệu!');
    }
    
    function detail_registration() {
        
    }

}
