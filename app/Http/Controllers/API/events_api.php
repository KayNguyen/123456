<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\AppApi;
use App\Http\Models\Event;
use Illuminate\Http\Request;

class events_api extends AppApi
{
    public function index($action = '')
    {
        $action = str_replace('-', '_', $action);
        if (method_exists($this, $action)) {
            return $this->$action();
        } else {
            return $this->list();
        }
    }

    public function list() {
        $id = \request('id', 0);
        if($id) {
            $lsObj = Event::getDetailEventById($id);
        }else {
            $lsObj = Event::getAllEventOfOrganizer();
        }

        if(!empty($lsObj)) {
            return $this->outputDone($lsObj, "Lấy dữ liệu thành công", 200);
        }
        return $this->outPutError('Không tìm thấy dữ liệu!', 200);
    }

    public function organizers() {
        $query_action = request('action');
        if($query_action == 'list') {
            $lsObj = Event::getAllEventOfOrganizer();
            return $this->outputDone($lsObj, "Lấy dữ liệu thành công", 200);
        }
    }
}
