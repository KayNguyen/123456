<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\AppApi;
use App\Http\Models\User;

class login_api extends AppApi
{
    public function login() {
    {
        $lastname = request('lastname', 0);
        $registration_code = request('registration_code', 0);

        if (!$lastname) {
            return $this->outputError("Bạn chưa nhập thông tin tài khoản", 401);
        }
        if (!$registration_code) {
            return $this->outputError("Bạn chưa nhập mã số đăng kí", 401);
        }
            $existUser = User::where(
                [
                    ['lastname', $lastname],
                    ['registration_code', $registration_code]
                ])->first();
            if (!$existUser) {
                return $this->outputError("Thông tin tài khoản không chính xác", 401);
            }        
            $existUser->login_token = strval($existUser['id']) .'__'.$existUser['id'].time().'xx'.'_'.sha1($existUser['id'].time().'xx'.'kayn'.$existUser['id'].'nochu');
            $existUser->save();
            $existUser['token'] = $existUser->login_token;
            return $this->outputDone($existUser, "Đăng nhập thành công", 200);
        }
    }
    public function logout() {
    {
        $token = request('token', '');
        if (!$token) {
            return $this->outputError("Thiếu token", 401);
        }
        $existUser = User::where('login_token', $token)->first();
        if (!$existUser) {
            return $this->outputError("Thông tin tài khoản không chính xác", 401);
        }        
        $existUser->login_token = "";
        $existUser->save();
        return $this->outputDone([], "Đăng xuất thành công", 200);
        }
    }
}
