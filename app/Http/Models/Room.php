<?php

namespace App\Http\Models;

use App\Http\Models\Event;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table  = 'rooms';

    public function channels(){
        return $this->belongsToMany('App\Http\Models\Channel', 'channel_id', 'id');
    }

    public function sessions(){
        return $this->hasMany('App\Http\Models\Session', 'room_id', 'id');
    }

    static function getRoomsByEventId($id = false)
    {
        if($id) {
            return Event::with('channels.rooms')->where('id', $id)->first()->toArray();
        }
        return false;
    }
}
