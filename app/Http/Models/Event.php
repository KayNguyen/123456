<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';
    public $timestamps = false;

    public function tickets(){
        return $this->hasMany('App\Http\Models\Ticket', 'event_id', 'id');
    }

    public function channels(){
        return $this->hasMany('App\Http\Models\Channel', 'event_id', 'id');
    }


    public function organizers(){
        return $this->hasOne('App\Http\Models\Admin', 'id', 'organizer_id');
    }

    public static function getAllEventOfOrganizer() {
        return self::get()->toArray();
    }

    public static function getAllEventByIdOrganizer($id) {
        return self::where('organizer_id', $id)->get()->toArray();
    }

    public static function getDetailEventById($id) {
        return self::with(['tickets', 'channels', 'channels.rooms', 'channels.rooms.sessions'])->where('id', $id)->first()->toArray();
    }

    public static function getDetailEventByAlias($alias) {
        $data = self::where('slug', $alias)->first();
        return $data;
    }

    public static function checkEventByIdOrganizer($id_admin, $id) {
        $model = self::find($id);
        if(!empty($model) && (int)$id_admin == $model['organizer_id']) {
            return true;
        }
        return false;

    }

     static function getAllSessionOfEventComing($event_id = 0) {
         $we = \DB::table('events')
                 ->leftJoin('channels', 'channels.event_id', '=', 'events.id')
                 ->leftJoin('rooms', 'rooms.channel_id', '=', 'channels.id')
                 ->join('sessions', 'sessions.room_id', '=', 'rooms.id')
                 ->leftJoin('event_tickets', 'event_tickets.event_id', '=', 'events.id')
                 ->where([
                     ['events.id', $event_id],
                     ['sessions.start', '>=', date('Y-m-d 00:00:00')],
                 ])
                 ->select('sessions.start', 'sessions.end', 'sessions.type', 'sessions.id', 'sessions.room_id')->orderByRaw('sessions.start ASC, sessions.end ASC')
                 ->get()->keyBy('start')->toArray();
         return $we;
     }

    static function getAllDetailEventOfOrganizer($organizer = 0, $event_id = 0)  {
        $we = self::with(['channels' => function($chan) {
            $chan->with(['rooms' => function($ro) {
                $ro->with('sessions')->get();
            }])->get();
        }, 'tickets'])->where('id', $event_id)->get()->toArray();
        return $we;
    }

    static function getAllUpcomingEvent($per = 10) {
        return self::paginate($per);
    }


}
