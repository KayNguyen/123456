<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $table = 'channels';

    public static function getChannelByIds($ids = []) {
        return self::whereIN('id', $ids)->get()->toArray();
    }

    public static function getRoomAndChannelByIds($ids = []) {
        return self::with('rooms')->whereIN('id', $ids)->get()->toArray();
    }

    public function rooms(){
        return $this->hasMany('App\Http\Models\Room', 'channel_id', 'id');
    }

    public function events(){
        return $this->belongsTo('App\Http\Models\Event', 'event_id', 'id');
    }
}
