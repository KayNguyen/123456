<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Models\Event;


class Session extends Model
{
    protected $table = 'sessions';

    public function rooms(){
        return $this->hasOne('App\Http\Models\Room', 'id', 'room_id');
    }

    static function getDetailSessionById($id) {
        return self::with('rooms')->find($id)->toArray();
    }

    static function checkIsYourSessionById($id) {
        return self::with('rooms')->where('id', $id)->get()->toArray();
    }
}
