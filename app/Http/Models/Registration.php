<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    protected $table = 'registrations';
    public $timestamps = false;
}
