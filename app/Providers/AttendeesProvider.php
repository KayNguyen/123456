<?php


namespace App\Providers;
use Illuminate\Auth\EloquentUserProvider as UserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;

class AttendeesProvider extends UserProvider
{
    public function validateCredentials(UserContract $user, array $credentials)
    {
        $plain = $credentials['registration_code'];
        return $this->hasher->check($plain, $user->getAuthPassword());
    }

}
