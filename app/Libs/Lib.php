<?php


namespace App\Libs;


class Lib
{
    static private $instance = false;
    public function __construct(){
        self::$instance = &$this;
    }

    public static function &getInstance() {
        if (!self::$instance) {
            new self();
        }
        return self::$instance;
    }

    public function setView($dir, $blade, $var = [], $render = false) {
        if ($dir) {
            \View::addLocation($dir);
            $location = '/views/';
        }else {
            $location = '';
        }
        $view = view($location .$blade, $var);
        if($render) {
            return $view->render();
        }
        return $view;
    }
}
