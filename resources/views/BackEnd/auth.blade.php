<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{ $site_title }}</title>

    <base href="./">
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('auth/assets/css/bootstrap.css') }}" rel="stylesheet">
    <!-- Custom styles -->
    <link href="{{ asset('auth/assets/css/custom.css') }}" rel="stylesheet">
</head>

<body>

<div class="container-fluid">
    <div class="row">
        @yield("CONTENT")
    </div>
</div>
</body>
</html>
